package org.bitbucket.larste.struktur;

import java.io.IOException;

import org.bitbucket.larste.struktur.presenter.ExamplePresenter;
import org.bitbucket.larste.struktur.presenter.MainPresenter;
import org.bitbucket.larste.struktur.service.ModelService;

import javafx.fxml.FXMLLoader;

public class AppFactory 
{
	private ModelService modelService;
	
	private MainPresenter mainPresenter;
	private ExamplePresenter examplePresenter;
	
	public MainPresenter getMainPresenter()
	{
		if ( mainPresenter == null )
		{
			try 
			{
				FXMLLoader loader = new FXMLLoader();
				loader.load(getClass().getResourceAsStream("view/Main.fxml"));
				mainPresenter = (MainPresenter) loader.getController();
				mainPresenter.setModelService(getModelService());
				
				mainPresenter.setExamplePresenter(getExamplePresenter());
			} 
			catch (IOException e) 
			{
				throw new RuntimeException("Unable to load Main.fxml", e);			
			}
		}
		return mainPresenter;
	}
	
	public ExamplePresenter getExamplePresenter()
	{
		if ( examplePresenter == null )
		{
			try 
			{
				FXMLLoader loader = new FXMLLoader();
				loader.load(getClass().getResourceAsStream("view/Example.fxml"));
				examplePresenter = (ExamplePresenter) loader.getController();
				examplePresenter.setModelService(getModelService());
				
				examplePresenter.setMainPresenter(getMainPresenter());
			} 
			catch (IOException e) 
			{
				throw new RuntimeException("Unable to load Example.fxml", e);			
			}
		}
		return examplePresenter;
	}
	
	public ModelService getModelService()
	{
		if ( modelService == null )
		{
			modelService = new ModelService();
		}
		
		return modelService;
	}
}