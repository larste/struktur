package org.bitbucket.larste.struktur.presenter;

import java.net.URL;
import java.util.ResourceBundle;

import org.bitbucket.larste.struktur.service.ModelService;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;

public class ExamplePresenter implements Initializable 
{
	@FXML private Node root;
	
	private MainPresenter mainPresenter;
	private ModelService modelService;
	
	public Node getView()
	{
		return root;
	}
	
	public void setMainPresenter(MainPresenter presenter)
	{
		mainPresenter = presenter;
	}
	
	public void setModelService(ModelService service)
	{
		modelService = service;
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		
	}

//	/**
//	 * @author zonski
//   * @url http://www.zenjava.com/2011/12/11/javafx-and-mvp-a-smorgasbord-of-design-patterns/
//	 * 	
//	 */
//    public void setContact(final Long id)
//    {
//        this.contactId = contactId;
//        firstNameField.setText("");
//        lastNameField.setText("");
//        final Task<Contact> loadTask = new Task<Contact>()
//        {
//            protected Contact call() throws Exception
//            {
//                return contactService.getContact(contactId);
//            }
//        };
//
//        loadTask.stateProperty().addListener(new ChangeListener<Worker.State>()
//        {
//            public void changed(ObservableValue<? extends Worker.State> source, Worker.State oldState, Worker.State newState)
//            {
//                if (newState.equals(Worker.State.SUCCEEDED))
//                {
//                    Contact contact = loadTask.getValue();
//                    firstNameField.setText(contact.getFirstName());
//                    lastNameField.setText(contact.getLastName());
//                }
//            }
//        });
//
//        new Thread(loadTask).start();
//    }
}