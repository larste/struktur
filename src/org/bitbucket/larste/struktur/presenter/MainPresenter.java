package org.bitbucket.larste.struktur.presenter;

import java.net.URL;
import java.util.ResourceBundle;

import org.bitbucket.larste.struktur.service.ModelService;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

public class MainPresenter implements Initializable 
{
	@FXML private Parent root;
	@FXML private BorderPane contentArea;
	
	private ModelService modelService;
	private ExamplePresenter examplePresenter;
	
	public Parent getView()
	{
		return root;
	}
	
	public void setModelService(ModelService service)
	{
		modelService = service;
	}
	
	public void setExamplePresenter(ExamplePresenter presenter)
	{
		examplePresenter = presenter;
	}
	
	public void showExample()
	{
		contentArea.setCenter(examplePresenter.getView());
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		
	}
}