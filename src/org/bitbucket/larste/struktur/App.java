package org.bitbucket.larste.struktur;
	
import org.bitbucket.larste.struktur.presenter.MainPresenter;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application 
{
	public static void main(String[] args) 
	{
		launch(args);
	}
	
	@Override
	public void start(Stage stage) 
	{
		AppFactory factory = new AppFactory();
		MainPresenter mainPresenter = factory.getMainPresenter();
		Scene scene = new Scene(mainPresenter.getView(), 800, 600);
		scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());
		stage.setScene(scene);
		stage.setTitle("Struktur");
		stage.show();
		mainPresenter.showExample();
	}
}
