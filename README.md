## Struktur
Struktur is a skeleton/starter template for JavaFX applications.
It's heavily influenced by [this](http://www.zenjava.com/2011/12/11/javafx-and-mvp-a-smorgasbord-of-design-patterns/) article by zonski.   

### License
Struktur is open-sourced software licensed under the [MIT licence](http://struktur.mit-license.org)